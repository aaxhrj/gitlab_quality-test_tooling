# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::Concerns::FindSetDri do
  let(:reporter) { Class.new { include GitlabQuality::TestTooling::Report::Concerns::FindSetDri }.new }

  before do
    stub_request(:get, "https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json")
      .to_return(status: 200, body:
        [
          { username: 'not-a-set',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/manager/">Manager, Create:IDE' },
          { username: 'set1',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Create:Code Review' },
          { username: 'set2',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Create:IDE' },
          { username: 'set3',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Manage:Import and Integrate' },
          { username: 'set4',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Systems: Gitaly' }
        ].to_json)
  end

  describe '#set_dri_via_group' do
    shared_examples 'SET finder' do
      it 'finds the single SET assigned to the product group' do
        expect(reporter.set_dri_via_group(product_group, test)).to eq(expected_set)
        expect(reporter.instance_variable_get(:@group_sets).size).to eq(1)
      end
    end

    context 'when there are multiple stage SETs' do
      let(:product_group) { 'code_review' }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb')
      end

      let(:expected_set) { 'set1' }

      it_behaves_like 'SET finder'
    end

    context 'when group name is an acronym' do
      let(:product_group) { 'ide' }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/3_create/ide/ide_spec.rb')
      end

      let(:expected_set) { 'set2' }

      it_behaves_like 'SET finder'
    end

    context 'when group name has spaces' do
      let(:product_group) { 'import_and_integrate' }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/1_manage/rate_limits_spec.rb')
      end

      let(:expected_set) { 'set3' }

      it_behaves_like 'SET finder'
    end

    context 'when group name is a single word' do
      let(:product_group) { 'gitaly' }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/12_systems/gitaly/gitaly_spec.rb')
      end

      let(:expected_set) { 'set4' }

      it_behaves_like 'SET finder'
    end
  end
end
