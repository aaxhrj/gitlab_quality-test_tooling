# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestResult::BaseTestResult do
  subject(:test_result) do
    Class.new(described_class) do
      def failures_from_exceptions; end
    end.new(report)
  end

  let(:report) { double }

  describe '#stage' do
    {
      nil => %w[qa/specs/features/sanity/version_spec.rb],
      'enablement' => %w[qa/specs/features/ee/browser_ui/enablement/elasticsearch/elasticsearch_reindexing_spec.rb],
      'monitor' => %w[qa/specs/features/ee/browser_ui/8_monitor/all_monitor_features_spec.rb],
      'geo' => %w[qa/specs/features/ee/api/geo/geo_nodes_spec.rb],
      'non_devops' => %w[qa/specs/features/browser_ui/non_devops/performance_bar_spec.rb],
      'manage' => %w[qa/specs/features/browser_ui/1_manage/project/dashboard_images_spec.rb],
      'create' => %w[qa/specs/features/api/3_create/gitaly/praefect_replication_queue_spec.rb]
    }.each do |expected_stage, file_paths|
      file_paths.each do |path|
        it "extracted #{expected_stage} from #{path}" do
          allow(test_result).to receive(:file).and_return(path)

          expect(test_result.stage).to eq(expected_stage)
        end
      end
    end
  end
end
