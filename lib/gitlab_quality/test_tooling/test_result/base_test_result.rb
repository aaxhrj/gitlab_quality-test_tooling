# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    module TestResult
      class BaseTestResult
        attr_reader :report

        def initialize(report)
          @report = report
        end

        def stage
          @stage ||= file[%r{(?:api|browser_ui)/(?:(?:\d+_)?(\w+))}, 1]
        end

        def name
          raise NotImplementedError
        end

        def file
          raise NotImplementedError
        end

        def skipped?
          raise NotImplementedError
        end

        def failures
          raise NotImplementedError
        end

        def failures?
          failures.any?
        end
      end
    end
  end
end
