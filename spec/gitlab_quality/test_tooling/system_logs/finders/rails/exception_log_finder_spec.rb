# frozen_string_literal: true

require_relative '../../shared_examples/log_finder_shared_examples'

RSpec.describe GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ExceptionLogFinder do
  let(:expected_log_name) { 'Rails Exceptions' }
  let(:expected_log_type) { GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ExceptionLog }
  let(:log_data_file_name) do
    'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs/gitlab-rails/exception_log_data.json'
  end

  describe '#find' do
    it_behaves_like 'log finder'
  end
end
