# frozen_string_literal: true

require 'json'

module GitlabQuality
  module TestTooling
    module Report
      module Concerns
        module FindSetDri
          def set_dri_via_group(product_group, test)
            parse_json_with_sets
            fetch_stage_sets(test)

            return @sets.sample['username'] if @stage_sets.empty?

            fetch_group_sets(product_group)

            if @group_sets.empty?
              @stage_sets.sample['username']
            else
              @group_sets.sample['username']
            end
          end

          private

          def parse_json_with_sets
            response = Support::HttpRequest.make_http_request(
              url: 'https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json'
            )
            @sets = JSON.parse(response.body).select { |user| user['role'].include?('software-engineer-in-test') }
          end

          def fetch_stage_sets(test)
            @stage_sets = @sets.select do |user|
              user['role'].include?(test.stage.split("_").map(&:capitalize).join(" "))
            end
          end

          def fetch_group_sets(product_group)
            @group_sets = @stage_sets.select do |user|
              user['role'].downcase.tr(' ', '_').include?(product_group)
            end
          end
        end
      end
    end
  end
end
