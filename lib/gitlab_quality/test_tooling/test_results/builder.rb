# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    module TestResults
      class Builder
        def initialize(file_glob)
          @file_glob = file_glob
        end

        def test_results_per_file
          Dir.glob(file_glob).each do |path|
            extension = File.extname(path)

            test_results =
              case extension
              when '.json'
                TestResults::JsonTestResults.new(path)
              when '.xml'
                TestResults::JUnitTestResults.new(path)
              else
                raise "Unknown extension #{extension}"
              end

            yield test_results
          end
        end

        private

        attr_reader :file_glob
      end
    end
  end
end
