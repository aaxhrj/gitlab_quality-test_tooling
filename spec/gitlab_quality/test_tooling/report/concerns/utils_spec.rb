# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::Concerns::Utils do
  let(:reporter) { Class.new { include GitlabQuality::TestTooling::Report::Concerns::Utils }.new }

  describe '#title_from_test' do
    context 'when title is less than 255 chars' do
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb',
          'full_description' => 'A test "with quotes" inside')
      end

      it 'returns well-formed title' do
        expect(reporter.title_from_test(test)).to eq("browser_ui/3_create/repository/add_new_branch_rule_spec.rb | A test with quotes inside")
      end
    end

    context 'when title is more than 255 chars' do
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb',
          'full_description' => "A test \"with quotes\" inside #{'a' * 255}")
      end

      it 'returns well-formed title' do
        expect(reporter.title_from_test(test)).to eq("#{"browser_ui/3_create/repository/add_new_branch_rule_spec.rb | A test with quotes inside #{'a' * 255}"[...252]}...")
      end
    end
  end

  describe '#new_issue_title' do
    let(:test) do
      GitlabQuality::TestTooling::TestResult::JsonTestResult.new('file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb',
        'full_description' => 'A test "with quotes" inside')
    end

    it 'returns well-formed title' do
      expect(reporter.new_issue_title(test)).to eq("browser_ui/3_create/repository/add_new_branch_rule_spec.rb | A test with quotes inside")
    end
  end

  describe '#partial_file_path' do
    context 'when path is a FOSS rails test' do
      it 'returns the path as-is' do
        file_path = 'spec/lib/gitlab/database/pg_depend_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a EE rails test' do
      it 'returns the path as-is' do
        file_path = 'ee/spec/lib/gitlab/database/pg_depend_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a FOSS E2E test' do
      it 'returns the path as-is' do
        file_path = 'browser_ui/1_manage/login/login_via_oauth_and_oidc_with_gitlab_as_idp_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a EE E2E test' do
      it 'returns the path as-is' do
        file_path = 'ee/browser_ui/1_manage/login/login_via_oauth_and_oidc_with_gitlab_as_idp_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a full-path FOSS E2E test' do
      it 'returns the path as-is' do
        file_path = 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq('browser_ui/3_create/repository/add_new_branch_rule_spec.rb')
      end
    end

    context 'when path is a full-path EE E2E test' do
      it 'returns the path as-is' do
        file_path = 'qa/specs/features/ee/browser_ui/3_create/repository/add_new_branch_rule_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq('ee/browser_ui/3_create/repository/add_new_branch_rule_spec.rb')
      end
    end

    context 'when the path is generic' do
      it 'returns the path as-is' do
        file_path = 'foobar/specs/this/is/my/test_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is unset' do
      it 'handles nil values' do
        file_path = nil
        expect(reporter.partial_file_path(file_path)).to be_nil
      end

      it 'handles an empty string' do
        file_path = ''
        expect(reporter.partial_file_path(file_path)).to eq('')
      end
    end
  end
end
