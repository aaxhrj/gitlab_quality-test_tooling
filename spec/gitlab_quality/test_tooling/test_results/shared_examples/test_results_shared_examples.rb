# frozen_string_literal: true

RSpec.shared_examples 'test results' do
  before do
    allow(File).to receive(:read).with('file').and_return(test_data)
  end

  subject { described_class.new('file') }

  context 'with a collection of test results' do
    it 'gets the test names' do
      expect(subject.map(&:name)).to eq(%w[test-name1 test-name2 test-name3])
    end

    it 'gets the test files' do
      expect(subject.map(&:file)).to eq(%w[test-file1 test-file2 test-file3])
    end

    it 'sets the testcase for each result' do
      subject.each do |test|
        test.testcase = test.name.reverse
      end

      expect(subject.map(&:testcase)).to eq(%w[1eman-tset 2eman-tset 3eman-tset])
    end
  end

  context 'with a skipped test' do
    it 'is recognised as skipped' do
      expect(subject.map(&:skipped?).one?).to be true
    end
  end

  context 'with a failed test' do
    it 'gets the error message' do
      expect(subject.first.failures.first['message']).to eq('Error: An Error Here')
    end

    it 'gets the stacktrace' do
      expect(subject.first.failures.first['stacktrace']).to eq("Failure/Error: raise \"An Error Here\"\n\nError:\nfail\nTest Stacktrace")
    end

    it 'ignores trace lines before the test starts' do
      expect(subject.to_a[1].failures.first['stacktrace'])
        .to eq("Failure/Error: raise \"An Error Here\"\n\nError:\nfail\ntest ends on line test-file2:10\ntest starts on line test-file2:1")
    end

    it 'shows the full trace if it does not include the test file' do
      expect(subject.to_a[1].failures[1]['stacktrace'])
        .to eq("Failure/Error: raise \"Another Error\"\n\nError2:\nfail\nTest Stacktrace Again\nbut without the file path")
    end
  end

  context 'with a failed test with multiple exceptions' do
    it 'gets the error messages' do
      expect(subject.to_a[1].failures.map { |e| e['message'] }).to eq(['Error: An Error Here', 'Error2: Another Error'])
    end

    it 'gets the stacktraces' do
      expect(subject.to_a[1].failures.map { |e| e['stacktrace'] })
        .to eq(
          [
            "Failure/Error: raise \"An Error Here\"\n\nError:\nfail\ntest ends on line test-file2:10\ntest starts on line test-file2:1",
            "Failure/Error: raise \"Another Error\"\n\nError2:\nfail\nTest Stacktrace Again\nbut without the file path"
          ]
        )
    end
  end
end
