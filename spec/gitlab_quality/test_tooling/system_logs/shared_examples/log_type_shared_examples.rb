# frozen_string_literal: true

RSpec.shared_examples 'log summary' do
  subject do
    described_class.new(
      JSON.parse(
        File.read(log_data_file_name),
        symbolize_names: true
      )
    )
  end

  let(:log_summary) { subject.summary }

  it 'returns the expected summary' do
    expect(log_summary).to eq(expected_summary)
  end
end
