# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestResult::JsonTestResult do
  subject(:test_result) { described_class.new(report) }

  let(:base_report) { { 'file_path' => 'file_path' } }
  let(:report_attrs) { {} }
  let(:report) { base_report.merge(report_attrs) }

  describe '#name' do
    let(:report_attrs) { { 'full_description' => 'foo' } }

    it { expect(test_result.name).to eq('foo') }
  end

  describe '#file' do
    let(:report_attrs) { { 'file_path' => 'foo' } }

    it { expect(test_result.file).to eq('foo') }

    context 'when report has a file_path with a ./ prefix' do
      let(:report_attrs) { { 'file_path' => './foo' } }

      it { expect(test_result.file).to eq('foo') }
    end
  end

  describe '#status' do
    let(:report_attrs) { { 'status' => 'passed' } }

    it { expect(test_result.status).to eq('passed') }
  end

  describe '#skipped?' do
    context 'when report status is not pending' do
      let(:report_attrs) { { 'status' => 'passed' } }

      it { expect(test_result.skipped?).to be(false) }
    end

    context 'when report status is pending' do
      let(:report_attrs) { { 'status' => 'pending' } }

      it { expect(test_result.skipped?).to be(true) }
    end
  end

  describe '#ci_job_url' do
    context 'when report has no ci_job_url' do
      it { expect(test_result.ci_job_url).to eq('') }
    end

    context 'when report has ci_job_url' do
      let(:report_attrs) { { 'ci_job_url' => 'ci_job_url' } }

      it { expect(test_result.ci_job_url).to eq('ci_job_url') }
    end
  end

  describe '#testcase' do
    context 'when report has no testcase' do
      it { expect(test_result.testcase).to eq('') }
    end

    context 'when report has testcase' do
      let(:report_attrs) { { 'testcase' => 'testcase' } }

      it { expect(test_result.testcase).to eq('testcase') }
    end
  end

  describe '#testcase=' do
    before do
      test_result.testcase = 'bar'
    end

    it { expect(test_result.testcase).to eq('bar') }
  end

  describe '#failure_issue' do
    context 'when report has no failure_issue' do
      it { expect(test_result.failure_issue).to be_nil }
    end

    context 'when report has failure_issue' do
      let(:report_attrs) { { 'failure_issue' => 'failure_issue' } }

      it { expect(test_result.failure_issue).to eq('failure_issue') }
    end
  end

  describe '#failure_issue=' do
    before do
      test_result.failure_issue = 'bar'
    end

    it { expect(test_result.failure_issue).to eq('bar') }
  end

  describe '#quarantine?' do
    context 'when report has no quarantine' do
      it { expect(test_result.quarantine?).to be(false) }
    end

    ["''", true, {}].each do |valid_value|
      context "when report has quarantine == #{valid_value}" do
        let(:report_attrs) { { 'quarantine' => valid_value } }

        it { expect(test_result.quarantine?).to be(true) }
      end
    end
  end

  describe '#quarantine_type' do
    context 'when report has no quarantine' do
      it { expect(test_result.quarantine_type).to be_nil }
    end

    context 'when report has quarantine type' do
      let(:report_attrs) { { 'quarantine' => { 'type' => 'flaky' } } }

      it { expect(test_result.quarantine_type).to eq('flaky') }
    end
  end

  describe '#quarantine_issue' do
    context 'when report has no quarantine' do
      it { expect(test_result.quarantine_issue).to be_nil }
    end

    context 'when report has quarantine type' do
      let(:report_attrs) { { 'quarantine' => { 'issue' => 'issue_url' } } }

      it { expect(test_result.quarantine_issue).to eq('issue_url') }
    end
  end

  describe '#screenshot?' do
    context 'when report has no screenshot' do
      it { expect(test_result.screenshot?).to be(false) }
    end

    context 'when report has a screenshot' do
      let(:report_attrs) { { 'screenshot' => {} } }

      it { expect(test_result.screenshot?).to be(true) }
    end

    context 'when report has a screenshot with image' do
      let(:report_attrs) { { 'screenshot' => { 'image' => 'screenshot_image' } } }

      it { expect(test_result.screenshot?).to be(true) }
    end
  end

  describe '#screenshot_image' do
    context 'when report has no screenshot' do
      it { expect(test_result.screenshot_image).to be_nil }
    end

    context 'when report has a screenshot' do
      let(:report_attrs) { { 'screenshot' => { 'image' => 'screenshot_image' } } }

      it { expect(test_result.screenshot_image).to eq('screenshot_image') }
    end
  end

  describe '#product_group' do
    context 'when report has no product_group' do
      it { expect(test_result.product_group).to eq('') }
    end

    context 'when report has a product_group' do
      let(:report_attrs) { { 'product_group' => 'foo' } }

      it { expect(test_result.product_group).to eq('foo') }
    end
  end

  describe '#product_group?' do
    context 'when report has no product_group' do
      it { expect(test_result.product_group?).to be(false) }
    end

    context 'when report has an empty product_group' do
      let(:report_attrs) { { 'product_group' => '' } }

      it { expect(test_result.product_group?).to be(false) }
    end

    context 'when report has a product_group' do
      let(:report_attrs) { { 'product_group' => 'foo' } }

      it { expect(test_result.product_group?).to be(true) }
    end
  end

  describe '#feature_category' do
    context 'when report has no feature_category' do
      it { expect(test_result.feature_category).to be_nil }
    end

    context 'when report has a feature_category' do
      let(:report_attrs) { { 'feature_category' => 'foo' } }

      it { expect(test_result.feature_category).to eq('foo') }
    end
  end

  describe '#run_time' do
    context 'when report has no run_time' do
      it { expect(test_result.run_time).to eq(0.0) }
    end

    context 'when report has a run_time' do
      let(:report_attrs) { { 'run_time' => '42.389' } }

      it { expect(test_result.run_time).to eq(42.39) }
    end
  end

  describe '#example_id' do
    context 'when report has no id' do
      it { expect(test_result.example_id).to be_nil }
    end

    context 'when report has a id' do
      let(:report_attrs) { { 'id' => 'foo' } }

      it { expect(test_result.example_id).to eq('foo') }
    end
  end

  describe '#line_number' do
    context 'when report has no line_number' do
      it { expect(test_result.line_number).to be_nil }
    end

    context 'when report has a line_number' do
      let(:report_attrs) { { 'line_number' => '42' } }

      it { expect(test_result.line_number).to eq('42') }
    end
  end

  describe '#failures?' do
    context 'when report has no failures' do
      it { expect(test_result.failures?).to be(false) }
    end

    context 'when report has a failures' do
      let(:report_attrs) do
        {
          'exceptions' => [
            {
              'class' => 'Error',
              'backtrace' => ['foo'],
              'message' => 'private_token=super_secret and another private_token=top_secret',
              'message_lines' => ['private_token=super_secret', 'another private_token=top_secret']
            }
          ]
        }
      end

      it { expect(test_result.failures?).to be(true) }
    end
  end

  describe '#failures' do
    context 'when report has no exceptions' do
      it { expect(test_result.failures).to eq([]) }
    end

    describe 'private_token redaction' do
      let(:report_attrs) do
        {
          'exceptions' => [
            {
              'class' => 'Error',
              'backtrace' => ['foo'],
              'message' => 'private_token=super_secret and another private_token=top_secret',
              'message_lines' => ['private_token=super_secret', 'another private_token=top_secret']
            }
          ]
        }
      end

      it 'redacts private_token' do
        expect(test_result.failures.first['message']).to eq('Error: ******** and another ********')
        expect(test_result.failures.first['message_lines']).to eq(['********', 'another ********'])
      end
    end

    context 'when report has exceptions with no backtrace' do
      let(:report_attrs) { { 'exceptions' => ['foo'] } }

      it 'returns true' do
        expect(test_result.failures).to eq([])
      end
    end

    context 'when report has exceptions with backtrace' do
      let(:exception_json) do
        {
          'class' => 'exception_class',
          'backtrace' => ['foo'],
          'message' => 'private_token=super_secret and another private_token=top_secret',
          'message_lines' => ['private_token=super_secret', 'another private_token=top_secret'],
          'correlation_id' => 'correlation_id'
        }
      end

      let(:report_attrs) { { 'exceptions' => [exception_json, exception_json] } }

      it 'returns expected failures hash' do
        expected_failure = {
          'message' => "exception_class: ******** and another ********",
          'message_lines' => ['********', 'another ********'],
          'stacktrace' => "********\nanother ********\nfoo",
          'correlation_id' => 'correlation_id'
        }

        expect(test_result.failures).to eq([expected_failure, expected_failure])
      end
    end
  end
end
