# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabIssueClient do
  let(:project) { 'project' }

  context 'with valid input' do
    let(:gitlab_client) { described_class.new(token: 'token', project: project) }

    context 'when the GitLab client is configured' do
      it 'passes the token to the GitLab client' do
        expect(Gitlab).to receive(:client).with(endpoint: anything, private_token: 'token')

        gitlab_client.__send__(:client)
      end

      it 'uses the default base API URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'https://gitlab.com/api/v4', private_token: anything)

        gitlab_client.__send__(:client)
      end
    end

    context 'when the base API URL is specified as an environment variable' do
      around do |example|
        ClimateControl.modify(GITLAB_API_BASE: 'http://another.gitlab.url') { example.run }
      end

      it 'uses the specified URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'http://another.gitlab.url', private_token: 'token')

        gitlab_client.__send__(:client)
      end
    end

    describe '#edit_issue_note' do
      it 'calls the #edit_issue_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:edit_issue_note).with(project, 1, 2, 'note')

        gitlab_client.edit_issue_note(issue_iid: 1, note_id: 2, note: 'note')
      end
    end

    describe '#find_issue_notes' do
      it 'calls the #issue_notes GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:issue_notes).with(project, 1, order_by: 'created_at', sort: 'asc')

        gitlab_client.find_issue_notes(iid: 1)
      end
    end

    describe '#assert_user_permission!' do
      let(:user_id) { 42 }
      let(:user) { Struct.new(:id, :username).new(user_id, 'john_doe') }
      let(:member) { Struct.new(:access_level).new(10) }

      before do
        allow(gitlab_client.__send__(:client)).to receive(:user).and_return(user)
      end

      it 'checks that the user has at least Reporter access to the project' do
        expect(gitlab_client.__send__(:client))
          .to receive(:team_member).with(project, user_id)
          .and_return(member)

        expect { gitlab_client.assert_user_permission! }
          .to output(
            "#{user.username} must have at least Reporter access to the project '#{project}' to use this feature. " \
            "Current access level: #{member.access_level}\n").to_stderr
          .and raise_error(SystemExit)
      end

      it 'checks that the user is a member of the project' do
        stub_const("Gitlab::Error::NotFound", RuntimeError)

        expect(gitlab_client.__send__(:client))
          .to receive(:team_member).with(project, user_id)
          .and_raise(Gitlab::Error::NotFound)

        expect { gitlab_client.assert_user_permission! }
          .to output("#{user.username} must be a member of the '#{project}' project.\n").to_stderr
          .and raise_error(SystemExit)
      end
    end
  end
end
