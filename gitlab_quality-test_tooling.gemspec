# frozen_string_literal: true

require_relative "lib/gitlab_quality/test_tooling/version"

Gem::Specification.new do |spec|
  spec.name    = "gitlab_quality-test_tooling"
  spec.version = GitlabQuality::TestTooling::VERSION
  spec.authors = ["GitLab Quality"]
  spec.email   = ["quality@gitlab.com"]

  spec.summary = "A collection of test-related tools."
  spec.description = "A collection of test-related tools."
  spec.homepage = "https://gitlab.com/gitlab-org/ruby/gems/gitlab_quality-test_tooling"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/-/releases"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:bin|spec)/|\.(?:git)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "climate_control", "~> 1.2"
  spec.add_development_dependency "gitlab-dangerfiles", "~> 3.8"
  spec.add_development_dependency "gitlab-styles", "~> 10.0"
  spec.add_development_dependency "guard-rspec", "~> 4.7"
  spec.add_development_dependency "lefthook", "~> 1.3"
  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "rspec", "~> 3.12"
  spec.add_development_dependency "simplecov", "~> 0.22"
  spec.add_development_dependency "simplecov-cobertura", "~> 2.1"
  spec.add_development_dependency "solargraph", "~> 0.41"
  spec.add_development_dependency "timecop", "~> 0.9.5"
  spec.add_development_dependency "webmock", "3.7.0"

  spec.add_runtime_dependency "activesupport", ">= 6.1", "< 7.2"
  spec.add_runtime_dependency 'amatch', "~> 0.4.1"
  spec.add_runtime_dependency "gitlab", "~> 4.19"
  spec.add_runtime_dependency "http", "~> 5.0"
  spec.add_runtime_dependency "nokogiri", "~> 1.10"
  spec.add_runtime_dependency "parallel", ">= 1", "< 2"
  spec.add_runtime_dependency "rainbow", ">= 3", "< 4"
  spec.add_runtime_dependency "table_print", "1.5.7"
  spec.add_runtime_dependency "zeitwerk", ">= 2", "< 3"
end
