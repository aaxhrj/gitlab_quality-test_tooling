# frozen_string_literal: true

# rubocop:disable RSpec/VerifiedDoubles
RSpec.describe GitlabQuality::TestTooling::Report::ReportResults do
  describe '#invoke!' do
    let(:test_results) { instance_double(GitlabQuality::TestTooling::TestResults::BaseTestResults) }
    let(:test_results_path) { 'test_results_path' }

    let(:test_file_path) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
    let(:test_description) { 'test-name' }
    let(:test) do
      GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
        'status' => 'failed',
        'testcase' => testcase_url,
        'file_path' => test_file_path,
        'full_description' => test_description
      )
    end

    let(:testcase_project_reporter) { instance_double(GitlabQuality::TestTooling::Report::ResultsInTestCases) }
    let(:gitlab_client) { instance_double(GitlabQuality::TestTooling::GitlabIssueClient) }
    let(:results_issue_project_reporter) { instance_double(GitlabQuality::TestTooling::Report::ResultsInIssues) }

    let(:testcase_iid) { 42 }
    let(:testcase_url) { "http://testcase.url/-/quality/test_cases/#{testcase_iid}" }
    let(:testcase) do
      double('Test case',
        web_url: testcase_url)
    end

    let(:result_issue_iid) { 99 }
    let(:result_issue_url) { "http://result-issue.url/-/issues/#{result_issue_iid}" }
    let(:result_issue) do
      double('Result issue',
        web_url: result_issue_url)
    end

    let(:test_case_project_token) { 'test_case_project_token' }
    let(:results_issue_project_token) { 'results_issue_project_token' }
    let(:test_case_project) { 'valid-test-case-project' }
    let(:results_issue_project) { 'valid-results-issue-project' }
    let(:files) { 'files' }

    subject do
      described_class.new(
        test_case_project_token: test_case_project_token,
        results_issue_project_token: results_issue_project_token,
        input_files: files,
        test_case_project: test_case_project,
        results_issue_project: results_issue_project)
    end

    before do
      test_results_builder = instance_double(GitlabQuality::TestTooling::TestResults::Builder)
      allow(test_results).to receive(:path).and_return(test_results_path)
      allow(test_results).to receive(:each).and_yield(test)

      allow(GitlabQuality::TestTooling::TestResults::Builder)
        .to receive(:new).with([files])
        .and_return(test_results_builder)
      allow(test_results_builder).to receive(:test_results_per_file).and_yield(test_results)

      allow(GitlabQuality::TestTooling::Report::ResultsInTestCases)
        .to receive(:new).with(
          token: test_case_project_token,
          input_files: files,
          project: test_case_project,
          dry_run: false)
        .and_return(testcase_project_reporter)
      allow(testcase_project_reporter).to receive(:gitlab).and_return(gitlab_client)

      allow(GitlabQuality::TestTooling::Report::ResultsInIssues)
        .to receive(:new).with(
          token: results_issue_project_token,
          input_files: files,
          project: results_issue_project,
          dry_run: false)
        .and_return(results_issue_project_reporter)
    end

    it 'delegates to classes' do
      expect(subject).to receive(:assert_input_files!).with([files])
      expect(subject.gitlab).to receive(:assert_user_permission!)
      expect(testcase_project_reporter)
        .to receive(:find_or_create_testcase).with(test)
        .and_return(testcase)

      expect(results_issue_project_reporter)
        .to receive(:get_related_issue).with(testcase, test)
        .and_return([result_issue, true])

      expect(testcase_project_reporter)
        .to receive(:add_result_issue_link_to_testcase).with(testcase, result_issue, test)

      expect(testcase_project_reporter)
        .to receive(:update_testcase).with(testcase, test)

      expect(results_issue_project_reporter)
        .to receive(:update_issue_labels).with(result_issue, test)
        .and_return(true)

      expect(results_issue_project_reporter)
        .to receive(:post_note).with(result_issue, test)
        .and_return(true)

      expect(test_results).to receive(:write)

      # subject.invoke!
      expect { subject.invoke! }
        .to output(
          <<~OUTPUT
              Reporting test results in `#{files}` as test cases in project `#{test_case_project}` and issues in project `#{results_issue_project}` via the API at `https://gitlab.com/api/v4`.
              Reporting tests in #{test_results_path}
              Reporting test: #{test_file_path} | #{test_description}
              Issue updated: #{result_issue.web_url}
          OUTPUT
        ).to_stdout
    end
  end
end
# rubocop:enable RSpec/VerifiedDoubles
