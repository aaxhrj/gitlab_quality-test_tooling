# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::SlowTestIssue do
  describe '#invoke!' do
    context 'when there are slow tests found' do
      let(:file_with_slow_test) { File.expand_path("fixtures/rspec-reports/rspec-4509805924.json", __dir__) }
      let(:project) { 'valid-project' }
      let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels) }
      let(:new_issue) { issue_class.new(0, 'http://new-issue.url') }
      let(:slow_test_path) { 'ee/spec/requests/api/visual_review_discussions_spec.rb' }
      let(:test_name) do
        'API::VisualReviewDiscussions when project is public behaves like accepting request without authentication behaves like handling merge request feedback creates a new note'
      end

      let(:report) { described_class.new(token: 'token', input_files: [file_with_slow_test], project: project) }

      before do
        allow(report.__send__(:gitlab)).to receive(:assert_user_permission!)
      end

      context 'with new slow test' do
        let(:labels_inference) { instance_double(GitlabQuality::TestTooling::LabelsInference) }

        let(:expected_new_issue_description) do
          <<~DESCRIPTION.chomp
          ### Test metadata (don't modify)

          | Field | Value |
          | ------ | ------ |
          | File | [`ee/spec/requests/api/visual_review_discussions_spec.rb#L177`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/spec/requests/api/visual_review_discussions_spec.rb#L177) |
          | Description | `API::VisualReviewDiscussions when project is public behaves like accepting request without authentication behaves like handling merge request feedback creates a new note` |
          | Test level | background_migration |
          | Hash | `56d5cd809c01070837b452622e5a88d272a09201a03e382f1ea2589f6315adb6` |
          | Duration | 28.98 seconds |
          | Expected duration | < 19.2 seconds |

          Slow tests were detected, please see the [test speed best practices guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#test-speed)
          to improve them. More context available about this issue in the [top slow tests guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#top-slow-tests).

          Add `allowed_to_be_slow: true` to the RSpec test if this is a legit slow test and close the issue.
          DESCRIPTION
        end

        before do
          allow(report.__send__(:gitlab)).to receive(:find_issues)
            .with(options: { state: 'opened', labels: described_class::SEARCH_LABELS })
            .and_return([])

          allow(GitlabQuality::TestTooling::LabelsInference).to receive(:new).and_return(labels_inference)
          allow(labels_inference).to receive(:infer_labels_from_product_group).and_return(Set.new)
          allow(labels_inference).to receive(:infer_labels_from_feature_category).and_return(Set.new)
        end

        it 'creates issue' do
          expect(report.__send__(:gitlab)).to receive(:create_issue)
            .with(
              title: "Slow test in #{slow_test_path} | #{test_name}",
              description: expected_new_issue_description,
              labels: described_class::NEW_ISSUE_LABELS.to_a,
              confidential: false
            )
            .and_return(new_issue)

          expect { report.invoke! }
            .to output(%r{Created new : http://new-issue.url.*}).to_stdout
        end
      end

      context 'with existing slow test issue' do
        let(:existing_issue) { issue_class.new(0, 'http://existing-issue.url', 'opened', "Slow test in #{slow_test_path} | #{test_name}", '', []) }

        before do
          allow(report.__send__(:gitlab)).to receive(:find_issues)
              .with(options: { state: 'opened', labels: described_class::SEARCH_LABELS })
              .and_return([existing_issue])
        end

        it 'does not create an issue' do
          expect(report.__send__(:gitlab)).not_to receive(:create_issue)

          expect { report.invoke! }
            .to output(%r{Existing issue link http://existing-issue.url*}).to_stdout

          expect { report.invoke! }
            .not_to output(%r{Created new : http://existing-issue.url*}).to_stdout
        end
      end
    end

    context 'when no are slow tests found' do
      let(:file_without_slow_test) { File.expand_path("fixtures/rspec-reports/rspec-4519210692.json", __dir__) }
      let(:project) { 'valid-project' }

      let(:report) { described_class.new(token: 'token', input_files: [file_without_slow_test], project: project) }

      before do
        allow(report.__send__(:gitlab)).to receive(:assert_user_permission!)
      end

      it 'does not create issue' do
        expect(report.__send__(:gitlab)).not_to receive(:create_issue)

        expect { report.invoke! }
            .to output(/Reporting 2 tests*/).to_stdout
        expect { report.invoke! }
          .not_to output(/Created new :*/).to_stdout
      end
    end
  end
end
