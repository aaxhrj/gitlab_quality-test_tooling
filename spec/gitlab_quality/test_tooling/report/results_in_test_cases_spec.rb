# frozen_string_literal: true

# rubocop:disable RSpec/VerifiedDoubles
RSpec.describe GitlabQuality::TestTooling::Report::ResultsInTestCases do
  let(:test_case_project) { 'valid-test-case-project' }
  let(:test_file_partial) { 'browser_ui/stage/test_spec.rb' }
  let(:test_file_path) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
  let(:test_description) { 'test-name' }
  let(:test_quarantine_data) { nil }
  let(:test) do
    GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
      'testcase' => testcase_url,
      'file_path' => test_file_path,
      'full_description' => test_description,
      'quarantine' => test_quarantine_data
    )
  end

  let(:testcase_iid) { 42 }
  let(:testcase_url) { "http://testcase.url/-/quality/test_cases/#{testcase_iid}" }
  let(:testcase_title) { subject.__send__(:title_from_test, test) }
  let(:testcase_description) { "#{subject.__send__(:new_issue_description, test)}\n\n" }
  let(:testcase_labels) { %w[Quality status::automated staging::passed] }

  let(:testcase) do
    double('Test case',
      iid: testcase_iid,
      state: 'opened',
      issue_type: 'test_case',
      title: testcase_title,
      description: testcase_description,
      labels: testcase_labels,
      web_url: testcase_url)
  end

  subject do
    described_class.new(token: 'token', input_files: 'files', project: test_case_project)
  end

  describe '#find_or_create_testcase' do
    context 'when a test case is linked in the given test' do
      context 'when testcase URL includes /-/issues' do
        let(:testcase_url) { "http://testcase.url/-/issues/#{testcase_iid}" }

        it 'fetches test case issue from the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues).with(options: { search: %("#{test_file_partial}" "#{test_description}") })
            .and_return([testcase])

          expect { subject.find_or_create_testcase(test) }
            .to output(%(\nPlease update #{testcase_url} to test case url\n)).to_stderr
            .and output("Found existing test_case: #{testcase_url}\n").to_stdout
        end
      end

      context 'when testcase URL does not include /-/issues' do
        it 'fetches test case from the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues).with(iid: testcase_iid)
            .and_return([testcase])

          subject.find_or_create_testcase(test)
        end

        context 'when test case title needs update' do
          let(:testcase_title) { 'old title' }

          it 'fetches test case from the API' do
            expect(subject.__send__(:gitlab))
              .to receive(:find_issues).with(iid: testcase_iid)
              .and_return([testcase])
            expect(subject.__send__(:gitlab))
              .to receive(:edit_issue).with(
                iid: testcase_iid,
                options: {
                  title: subject.__send__(:title_from_test, test)
                }
              )
              .and_return([testcase])

            expect { subject.find_or_create_testcase(test) }
              .to output(/test_case title needs to be updated from 'old title' to '.+'/).to_stderr
          end
        end

        context 'when test case description needs update' do
          let(:testcase_description) { 'old description' }

          it 'fetches test case from the API' do
            expect(subject.__send__(:gitlab))
              .to receive(:find_issues).with(iid: testcase_iid)
              .and_return([testcase])
            expect(subject.__send__(:gitlab))
              .to receive(:edit_issue).with(
                iid: testcase_iid,
                options: {
                  description: "#{subject.__send__(:new_issue_description, test)}\n\n"
                }
              )
              .and_return([testcase])

            expect { subject.find_or_create_testcase(test) }
              .to output(/test_case description needs to be updated from 'old description' to '.+'/).to_stderr
          end
        end
      end
    end

    context 'when no test case is linked in the given test' do
      let(:testcase_url) { nil }

      context 'when an issue is found' do
        it 'fetches test case issue from the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues).with(options: { search: %("#{test_file_partial}" "#{test_description}") })
            .and_return([testcase])

          expect { subject.find_or_create_testcase(test) }
            .to output("Found existing test_case: #{testcase_url}\n").to_stdout
        end
      end

      context 'when no issue is found' do
        let(:new_testcase) { double('Test case', web_url: 'path/to/issues/123') }

        it 'creates a new test case via the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues)
            .and_return([])
          expect(subject.__send__(:gitlab))
            .to receive(:create_issue).with(
              title: testcase_title,
              description: testcase_description.strip,
              labels: %w[Quality status::automated],
              issue_type: 'test_case',
              confidential: false
            )
            .and_return(new_testcase)

          expect { subject.find_or_create_testcase(test) }
            .to output("Created new test_case: path/to/quality/test_cases/123\n").to_stdout
        end
      end
    end
  end

  describe '#add_result_issue_link_to_testcase' do
    let(:result_issue) { double('Result issue', web_url: 'result_issue_url') }

    it 'adds the test case results section template and result issue URL at the bottom of the description' do
      expect(subject.__send__(:gitlab))
        .to receive(:edit_issue).with(
          iid: testcase_iid,
          options: {
            description: "#{testcase_description}#{described_class::TEST_CASE_RESULTS_SECTION_TEMPLATE}\n\n#{result_issue.web_url}"
          }
        )
        .and_return([testcase])

      expect { subject.add_result_issue_link_to_testcase(testcase, result_issue, test) }
        .to output("Added results issue result_issue_url link to test case #{testcase_url}\n").to_stdout
    end

    context 'when description already includes TEST_CASE_RESULTS_SECTION_TEMPLATE' do
      let(:testcase_description) { "#{subject.__send__(:new_issue_description, test)}\n\n#{described_class::TEST_CASE_RESULTS_SECTION_TEMPLATE}" }

      it 'adds the result issue URL at the bottom of the description' do
        expect(subject.__send__(:gitlab))
          .to receive(:edit_issue).with(
            iid: testcase_iid,
            options: {
              description: "#{testcase_description}\n\n#{result_issue.web_url}"
            }
          )
          .and_return([testcase])

        expect { subject.add_result_issue_link_to_testcase(testcase, result_issue, test) }
          .to output("Added results issue result_issue_url link to test case #{testcase_url}\n").to_stdout
      end
    end
  end

  describe '#update_testcase' do
    around do |example|
      ClimateControl.modify(CI_PROJECT_NAME: 'staging') { example.run }
    end

    context 'when test case labels need update' do
      let(:testcase_labels) { [] }

      it 'updates test case labels' do
        expect(subject.__send__(:gitlab))
          .to receive(:edit_issue).with(
            iid: testcase_iid,
            options: {
              labels: %w[Quality status::automated staging::passed]
            }
          )
          .and_return([testcase])

        expect { subject.update_testcase(testcase, test) }
          .to output("Labels updated for test case #{testcase_url}.\n").to_stdout
      end
    end

    shared_examples 'quarantine section update' do
      let(:quarantine_issue_url) { 'https://quarantine/issues/567' }
      let(:test_quarantine_data) { { 'issue' => quarantine_issue_url, 'type' => 'flaky' } }

      it 'updates the quarantine link' do
        expect(subject.__send__(:gitlab))
          .to receive(:edit_issue).with(
            iid: testcase_iid,
            options: {
              labels: %w[Quality status::automated quarantine quarantine::flaky staging::passed]
            }
          )
          .and_return([testcase])
        expect(subject.__send__(:gitlab))
          .to receive(:edit_issue).with(
            iid: testcase_iid,
            options: {
              description: /## Quarantine issue\n\n#{quarantine_issue_url}/
            }
          )
          .and_return([testcase])

        expect { subject.update_testcase(testcase, test) }
          .to output("Labels updated for test case #{testcase_url}.\nQuarantine section updated for test case #{testcase_url}.\n").to_stdout
      end
    end

    context 'when quarantine link is not present in test case' do
      let(:testcase_description) { "" }

      it_behaves_like 'quarantine section update'
    end

    context 'when quarantine link is not up-to-date in test case' do
      let(:testcase_description) { "## Quarantine issue\n\nhttps://quarantine/issues/789" }

      it_behaves_like 'quarantine section update'
    end
  end
end
# rubocop:enable RSpec/VerifiedDoubles
