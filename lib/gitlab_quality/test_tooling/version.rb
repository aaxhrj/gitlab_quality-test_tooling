# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    VERSION = "1.4.0"
  end
end
