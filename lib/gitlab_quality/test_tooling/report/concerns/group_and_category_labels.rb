# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    module Report
      module Concerns
        module GroupAndCategoryLabels
          def labels_inference
            @labels_inference ||= GitlabQuality::TestTooling::LabelsInference.new
          end

          def new_issue_labels(test)
            puts "  => [DEBUG] product_group: #{test.product_group}; feature_category: #{test.feature_category}"

            new_labels = self.class::NEW_ISSUE_LABELS +
              labels_inference.infer_labels_from_product_group(test.product_group) +
              labels_inference.infer_labels_from_feature_category(test.feature_category)
            up_to_date_labels(test: test, new_labels: new_labels)
          end
        end
      end
    end
  end
end
