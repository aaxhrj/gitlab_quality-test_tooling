# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::LabelsInference do
  describe 'JSON parsing error' do
    before do
      stub_request(:get, 'https://about.gitlab.com/groups.json')
        .to_return(body: '{ "source_code": }')
    end

    it 'fails Set.new gracefully' do
      expect(described_class.new.infer_labels_from_product_group('source_code'))
        .to eq(Set.new)
    end
  end

  describe '#infer_labels_from_product_group' do
    before do
      stub_request(:get, 'https://about.gitlab.com/groups.json')
        .to_return(body:
        {
          source_code: { label: 'group::source code' }
        }.to_json)
    end

    context 'when the group is found' do
      it 'infer the group label from the group name' do
        expect(described_class.new.infer_labels_from_product_group('source_code'))
          .to eq(['group::source code'].to_set)
      end
    end

    context 'when the group is nil' do
      it 'returns Set.new' do
        expect(described_class.new.infer_labels_from_product_group(nil))
          .to eq(Set.new)
      end
    end

    context 'when the group is not found' do
      it 'returns Set.new' do
        expect(described_class.new.infer_labels_from_product_group('unknown'))
          .to eq(Set.new)
      end
    end
  end

  describe '#infer_labels_from_feature_category' do
    before do
      stub_request(:get, 'https://about.gitlab.com/categories.json')
        .to_return(body:
        {
          source_code_management: { label: 'Category:Source Code Management', group: 'source_code' }
        }.to_json)
      stub_request(:get, 'https://about.gitlab.com/groups.json')
        .to_return(body:
        {
          source_code: { label: 'group::source code' }
        }.to_json)
    end

    context 'when the category is found' do
      it 'infer the group label from the group name' do
        expect(described_class.new.infer_labels_from_feature_category('source_code_management'))
          .to eq(['Category:Source Code Management', 'group::source code'].to_set)
      end
    end

    context 'when the category is nil' do
      it 'returns Set.new' do
        expect(described_class.new.infer_labels_from_feature_category(nil))
          .to eq(Set.new)
      end
    end

    context 'when the category is not found' do
      it 'returns Set.new' do
        expect(described_class.new.infer_labels_from_feature_category('unknown'))
          .to eq(Set.new)
      end
    end
  end
end
