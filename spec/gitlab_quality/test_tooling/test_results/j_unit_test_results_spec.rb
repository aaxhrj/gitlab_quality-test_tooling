# frozen_string_literal: true

require_relative 'shared_examples/test_results_shared_examples'

RSpec.describe GitlabQuality::TestTooling::TestResults::JUnitTestResults do
  it_behaves_like 'test results' do
    let(:test_data) do
      <<~XML
        <testsuite>
          <testcase name="test-name1" file="test-file1">
            <failure message="An Error Here" type="Error">Failure/Error: raise "An Error Here"

            Error:
            fail
            Test Stacktrace</failure>
          </testcase>
          <testcase name="test-name2" file="test-file2">
            <failure message="An Error Here" type="Error">Failure/Error: raise "An Error Here"

              Error:
              fail
              test ends on line test-file2:10
              test starts on line test-file2:1
              rspec and other unimportant code</failure>
            <failure message="Another Error" type="Error2">Failure/Error: raise "Another Error"

            Error2:
            fail
            Test Stacktrace Again
            but without the file path</failure>
          </testcase>
          <testcase name="test-name3" file="test-file3"><skipped/></testcase>
        </testsuite>
      XML
    end
  end
end
