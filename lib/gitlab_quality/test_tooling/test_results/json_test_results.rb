# frozen_string_literal: true

require 'json'

module GitlabQuality
  module TestTooling
    module TestResults
      class JsonTestResults < BaseTestResults
        def write
          json = results.merge('examples' => testcases.map(&:report))

          File.write(path, JSON.pretty_generate(json))
        end

        private

        def parse
          JSON.parse(File.read(path))
        end

        def process
          results['examples'].map do |test|
            GitlabQuality::TestTooling::TestResult::JsonTestResult.new(test)
          end
        end
      end
    end
  end
end
