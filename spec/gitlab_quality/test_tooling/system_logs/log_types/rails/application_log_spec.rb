# frozen_string_literal: true

require_relative '../../shared_examples/log_type_shared_examples'

RSpec.describe GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApplicationLog do
  let(:log_data_file_name) do
    'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs/gitlab-rails/application_log_data.json'
  end

  let(:expected_summary) do
    {
      severity: "ERROR",
      time: "2023-01-25T15:49:59.337Z",
      correlation_id: "foo123",
      message: "Test error message",
      exception_class: "FooBarClass",
      exception_message: "Test exception message",
      meta_user: "root",
      meta_project: "gitlab-qa-sandbox-group/foo-group/bar-project",
      meta_caller_id: "POST /api/:version/foo/bar"
    }
  end

  describe '#summary' do
    it_behaves_like 'log summary'
  end
end
