#!/usr/bin/env ruby
# frozen_string_literal: true

require "bundler/setup"
require "optparse"

require_relative "../lib/gitlab_quality/test_tooling"

params = {}

options = OptionParser.new do |opts|
  opts.banner = "Usage: #{$PROGRAM_NAME} [options]"

  opts.on('-i', '--input-files INPUT_FILES', String, 'RSpec report files (JUnit XML)') do |input_files|
    params[:input_files] = input_files
  end

  opts.on_tail('-v', '--version', 'Show the version') do
    require_relative "../lib/gitlab_quality/test_tooling/version"
    puts "#{$PROGRAM_NAME} : #{GitlabQuality::TestTooling::VERSION}"
    exit
  end

  opts.on_tail('-h', '--help', 'Show the usage') do
    puts "Purpose: Prepare separate reports for each DevOps stage from the provided RSpec report files (JUnit XML)"
    puts opts
    exit
  end

  opts.parse(ARGV)
end

if params.any?
  GitlabQuality::TestTooling::Report::PrepareStageReports.new(**params).invoke!
else
  puts options
  exit 1
end
