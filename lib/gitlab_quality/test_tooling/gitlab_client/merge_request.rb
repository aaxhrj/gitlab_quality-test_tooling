# frozen_string_literal: true

require 'gitlab'

module GitlabQuality
  module TestTooling
    module GitlabClient
      class MergeRequest
        def initialize(token:, project:, merge_request_iid:)
          @token = token
          @project = project
          @merge_request_iid = merge_request_iid
        end

        def find_merge_request
          client.merge_request_changes(project, merge_request_iid)
        end

        def merge_request_changed_files
          find_merge_request["changes"].map do |change|
            change["new_path"]
          end
        end

        def find_note(body:)
          client.merge_request_notes(project, merge_request_iid, per_page: 100).auto_paginate.find do |mr_note|
            mr_note['body'] =~ /#{body}/
          end
        end

        def create_note(note:)
          client.create_merge_request_note(project, merge_request_iid, note)
        end

        def update_note(id:, note:)
          client.edit_merge_request_note(project, merge_request_iid, id, note)
        end

        private

        attr_reader :project, :token, :merge_request_iid

        def client
          @client ||= Gitlab.client(
            endpoint: Runtime::Env.gitlab_api_base,
            private_token: token
          )
        end
      end
    end
  end
end
