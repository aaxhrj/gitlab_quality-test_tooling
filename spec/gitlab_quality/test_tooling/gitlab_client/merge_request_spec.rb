# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::MergeRequest do
  let(:project) { 'project' }
  let(:merge_request_iid) { 10 }

  context 'with valid input' do
    let(:gitlab_client) { described_class.new(token: 'token', project: project, merge_request_iid: merge_request_iid) }

    context 'when the GitLab client is configured' do
      it 'passes the token to the GitLab client' do
        expect(Gitlab).to receive(:client).with(endpoint: anything, private_token: 'token')

        gitlab_client.__send__(:client)
      end

      it 'uses the default base API URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'https://gitlab.com/api/v4', private_token: anything)

        gitlab_client.__send__(:client)
      end
    end

    context 'when the base API URL is specified as an environment variable' do
      around do |example|
        ClimateControl.modify(GITLAB_API_BASE: 'http://another.gitlab.url') { example.run }
      end

      it 'uses the specified URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'http://another.gitlab.url', private_token: 'token')

        gitlab_client.__send__(:client)
      end
    end

    describe '#find_merge_request' do
      it 'calls the #merge_request_changes GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:merge_request_changes).with(project, merge_request_iid)

        gitlab_client.find_merge_request
      end
    end

    describe '#find_note' do
      let(:gitlab_response) { double }

      before do
        allow(gitlab_response).to receive(:auto_paginate).and_return([])
      end

      it 'calls #merge_request_notes GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:merge_request_notes).with(project, merge_request_iid, per_page: 100)
          .and_return(gitlab_response)

        gitlab_client.find_note(body: 'note')
      end
    end

    describe '#create_note' do
      let(:note) { 'Slow test note' }

      it 'calls #create_merge_request_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:create_merge_request_note).with(project, merge_request_iid, note)

        gitlab_client.create_note(note: note)
      end
    end

    describe '#update_note' do
      let(:note) { 'Slow test note' }

      it 'calls #create_merge_request_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:edit_merge_request_note).with(project, merge_request_iid, 10, note)

        gitlab_client.update_note(id: 10, note: note)
      end
    end
  end
end
