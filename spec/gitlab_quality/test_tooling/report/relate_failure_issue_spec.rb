# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::RelateFailureIssue do
  describe '#invoke!' do
    let(:project) { 'valid-project' }
    let(:test_name) { 'Manage Users API GET /users' }
    let(:test_file_partial) { 'api/1_manage/users_spec.rb' }
    let(:test_file_full) { "qa/specs/features/#{test_file_partial}" }
    let(:test_hash) { OpenSSL::Digest::SHA256.hexdigest(test_file_full + test_name) }
    let(:testcase_url) { 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/460' }
    let(:product_group_tag) { nil }
    let(:ci_job_url) { 'https://gitlab.com/gitlab-org/gitlab-qa/-/jobs/806591854' }
    let(:file_upload_class) { Struct.new(:markdown) }
    let(:file_upload) { file_upload_class.new("![failure_screenshot](/uploads/failure_screenshot.png)") }
    let(:failure_class) { 'Support::Repeater::WaitExceededError' }
    let(:failure_message) { 'Page did not fully load. This could be due to an unending async request or loading icon.' }
    let(:failure_lines) do
      [
        'Failure/Error: Page::Project::Wiki::Edit.perform(&:click_submit)',
        '',
        'Support::Repeater::WaitExceededError:',
        '  Page did not fully load. This could be due to an unending async request or loading icon.'
      ]
    end

    let(:test_data) do
      <<~JSON
        {
          "examples": [
            {
              "id":"#{test_file_full}[1:1:1]",
              "description":"GET /users",
              "full_description": "#{test_name}",
              "status":"failed",
              "file_path":"#{test_file_full}",
              "line_number":11,
              "run_time":0.31676485,
              "pending_message":null,
              "testcase":"#{testcase_url}",
              "quarantine":null,
              "screenshot": { "image": "failure_screenshot.png"},
              "product_group":"#{product_group_tag}",
              "ci_job_url":"#{ci_job_url}",
              "exceptions":[
                {
                  "class":"#{failure_class}",
                  "message":"#{failure_message}",
                  "message_lines":#{failure_lines.to_json},
                  "correlation_id":"foo123",
                  "backtrace":[
                    "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                    "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                  ]
                }
              ]
            }
          ]
        }
      JSON
    end

    let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels) }
    let(:new_issue) { issue_class.new(0, 'http://new-issue.url') }
    let(:base_issue_labels) { %w[QA Quality] }
    let(:issue_stack_trace) { failure_lines.join("\n") }
    let(:test_metadata_section) do
      <<~TEST_METADATA_SECTION.chomp
      ### Test metadata (don't modify)

      | Field | Value |
      | ------ | ------ |
      | File | [`qa/#{test_file_full}#L11`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/#{test_file_full}#L11) |
      | Description | `#{test_name}` |
      | Test level |  |
      | Hash | `#{test_hash}` |
      | Duration | 0.32 seconds |
      | Expected duration | < 240 seconds |
      | Test case | #{testcase_url} |
      TEST_METADATA_SECTION
    end

    let(:stack_trace_section) do
      <<~STACK_TRACE_SECTION.chomp
      ### Stack trace

      ```
      #{issue_stack_trace}
      ```
      STACK_TRACE_SECTION
    end

    let(:screenshot_section) do
      <<~SCREENSHOT_SECTION.chomp
      ### Screenshot

      #{file_upload.markdown}
      SCREENSHOT_SECTION
    end

    let(:system_logs_section) do
      <<~SYSTEM_LOGS_SECTION.chomp
        ### System Logs

        #### Rails Exceptions

        <details><summary>Click to expand</summary>
        ```
        {
           "severity": "ERROR",
           "correlation_id": "foo123",
           "time": "2023-01-25T15:50:21.738Z",
           "message": "This is a test error message"
        }
        ```
        </details>
      SYSTEM_LOGS_SECTION
    end

    let(:reports_section) do
      <<~DESCRIPTION.chomp
      ### Reports (1)

      1. 2023-06-07: #{ci_job_url} (pipeline_url)
      DESCRIPTION
    end

    let(:expected_new_issue_description) do
      <<~DESCRIPTION
      #{test_metadata_section}

      #{stack_trace_section}

      #{screenshot_section}

      #{system_logs_section}

      #{reports_section}
      DESCRIPTION
    end

    let(:input_files_pattern) { 'files' }
    let(:system_logs_pattern) { 'gitlab-qa-foo-*/**/logs' }
    let(:system_logs_dir) { ['gitlab-qa-foo-123/gitlab-ee-123/logs'] }

    let(:system_logs_formatter) do
      instance_double(
        GitlabQuality::TestTooling::SystemLogs::SystemLogsFormatter,
        system_logs_summary_markdown: system_logs_section
      )
    end

    let(:test_file) { 'file.json' }
    let(:issues) { [] }
    let(:issue_description) do
      <<~DESCRIPTION
      #{test_metadata_section}

      #{stack_trace_section}
      DESCRIPTION
    end

    let(:labels_inference) { instance_double(GitlabQuality::TestTooling::LabelsInference) }

    shared_examples 'screenshot not included' do
      let(:expected_new_issue_description) do
        <<~DESCRIPTION
        #{test_metadata_section}

        #{stack_trace_section}

        #{system_logs_section}

        #{reports_section}
        DESCRIPTION
      end

      it 'does not include a screenshot' do
        expect(subject.__send__(:gitlab)).not_to receive(:upload_file)
        expect(subject.__send__(:gitlab)).to receive(:create_issue)
          .with(
            title: "Failure in #{test_file_partial} | #{test_name}",
            description: expected_new_issue_description,
            labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'],
            issue_type: 'issue',
            confidential: false
          )
          .and_return(new_issue)

        expect { subject.invoke! }.to output.to_stdout
      end
    end

    shared_examples 'issue created with failure class & message' do
      let(:issue_stack_trace) { "#{failure_class}: #{failure_message}" }

      it 'uses the message to create an issue' do
        expect(subject.__send__(:gitlab)).to receive(:upload_file).and_return(file_upload)
        expect(subject.__send__(:gitlab)).to receive(:create_issue)
          .with(
            title: "Failure in #{test_file_partial} | #{test_name}",
            description: expected_new_issue_description,
            issue_type: 'issue',
            labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'],
            confidential: false
          )
          .and_return(new_issue)

        expect { subject.invoke! }
          .to output(%r{Created new issue: http://new-issue.url.*}).to_stdout
      end
    end

    shared_examples 'new issue created' do
      it 'creates issue' do
        expect(subject.__send__(:gitlab)).to receive(:upload_file).and_return(file_upload)
        expect(subject.__send__(:gitlab)).to receive(:create_issue).and_return(new_issue)
        expect(subject).not_to receive(:update_reports)

        expect { subject.invoke! }
          .to output(%r{Created new issue: http://new-issue.url.*}).to_stdout
      end
    end

    shared_examples 'error(s) can be ignored' do
      it 'skips reporting' do
        expect(subject.__send__(:gitlab)).not_to receive(:create_issue)

        expect { subject.invoke! }
          .to output(/Failure reporting skipped because the errors included: #{Regexp.escape(failure_reason)}/).to_stdout
      end
    end

    around do |example|
      ClimateControl.modify(
        CI_PROJECT_NAME: 'staging',
        CI_PIPELINE_URL: 'pipeline_url') do
        Timecop.freeze(Time.utc(2023, 6, 7, 15)) { example.run }
      end
    end

    before do
      allow(Dir).to receive(:glob).with(system_logs_pattern).and_return(system_logs_dir)

      allow(subject).to receive(:assert_input_files!)
      allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!)

      allow(Dir).to receive(:glob).with([input_files_pattern]).and_return([test_file])
      allow(File).to receive(:read).with(test_file).and_return(test_data)

      allow(GitlabQuality::TestTooling::SystemLogs::SystemLogsFormatter).to receive(:new).and_return(system_logs_formatter)

      allow(File).to receive(:write)
      allow(subject.__send__(:gitlab)).to receive(:find_issues)
        .with(options: { labels: base_issue_labels + %w[test] })
        .and_return(issues)
      allow(subject.__send__(:gitlab)).to receive(:find_issues)
        .with(options: { labels: base_issue_labels + %w[test failure::new], created_after: anything })
        .and_return(issues)

      allow(GitlabQuality::TestTooling::LabelsInference).to receive(:new).and_return(labels_inference)
      allow(labels_inference).to receive(:infer_labels_from_product_group).and_return(Set.new)
      allow(labels_inference).to receive(:infer_labels_from_feature_category).and_return(Set.new)

      subject.instance_variable_set(:@commented_issue_list, Set.new)
    end

    subject do
      described_class.new(token: 'token', input_files: input_files_pattern, project: project,
        system_logs: system_logs_pattern, base_issue_labels: base_issue_labels)
    end

    context 'when no failures are present' do
      let(:test_data) do
        <<~JSON
          {
            "examples": [
              {
                "full_description": "#{test_name}",
                "file_path": "#{test_file_full}"
              }
            ]
          }
        JSON
      end

      it 'does not find the issue' do
        expect { subject.invoke! }.not_to output(/Found issue/).to_stdout
      end
    end

    describe 'NEW_ISSUE_LABELS' do
      it { expect(described_class::NEW_ISSUE_LABELS).to eq(Set.new(%w[test failure::new priority::2])) }
    end

    context 'when no issue is found' do
      context 'when creating a new issue' do
        shared_examples 'creates the issue in the provided project and posts the failed job url' do
          specify do
            expect(subject.__send__(:gitlab)).to receive(:upload_file).and_return(file_upload)
            expect(subject.__send__(:gitlab)).to receive(:create_issue)
              .with(
                title: "Failure in #{test_file_partial} | #{test_name}",
                description: expected_new_issue_description,
                labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'],
                issue_type: 'issue',
                confidential: false
              )
              .and_return(new_issue)

            expect { subject.invoke! }.to output.to_stdout
          end
        end

        let(:issue_description_without_system_logs) do
          <<~DESCRIPTION
          #{test_metadata_section}

          #{stack_trace_section}

          #{screenshot_section}

          #{reports_section}
          DESCRIPTION
        end

        context 'when no system logs exist' do
          let(:system_logs_dir) { [] }
          let(:expected_new_issue_description) { issue_description_without_system_logs }

          it_behaves_like 'creates the issue in the provided project and posts the failed job url'
        end

        context 'when system logs exist' do
          context 'when failure contains a correlation id' do
            it_behaves_like 'creates the issue in the provided project and posts the failed job url'
          end

          context 'when failure does not contain a correlation id' do
            let(:expected_new_issue_description) { issue_description_without_system_logs }

            let(:test_data) do
              <<~JSON
                {
                    "examples": [
                      {
                        "id":"#{test_file_full}[1:1:1]",
                        "description":"GET /users",
                        "full_description": "#{test_name}",
                        "status":"failed",
                        "file_path":"#{test_file_full}",
                        "line_number":11,
                        "run_time":0.31676485,
                        "pending_message":null,
                        "testcase":"#{testcase_url}",
                        "quarantine":null,
                        "screenshot": { "image": "failure_screenshot.png"},
                        "product_group":"#{product_group_tag}",
                        "ci_job_url":"#{ci_job_url}",
                        "exceptions":[
                          {
                            "class":"#{failure_class}",
                            "message":"#{failure_message}",
                            "message_lines":#{failure_lines.to_json},
                            "backtrace":[
                              "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                              "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                            ]
                          }
                        ]
                      }
                    ]
                }
              JSON
            end

            it_behaves_like 'creates the issue in the provided project and posts the failed job url'
          end
        end

        it 'includes the failure screenshot in the description' do
          expect(subject.__send__(:gitlab)).to receive(:upload_file)
            .with(file_fullpath: 'failure_screenshot.png')
            .and_return(file_upload)

          expect(subject.__send__(:gitlab)).to receive(:create_issue)
            .with(
              title: "Failure in #{test_file_partial} | #{test_name}",
              description: expected_new_issue_description,
              labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'],
              issue_type: 'issue',
              confidential: false
            )
            .and_return(new_issue)

          expect { subject.invoke! }.to output.to_stdout
        end

        context 'when the error is a 500 Internal Server Error' do
          let(:failure_lines) { ["Failed to GET group - (500): { 'message' : '500 Internal Server Error' }."] }

          it_behaves_like 'screenshot not included'
        end

        context 'when the error is from fabricate_via_api!' do
          let(:failure_lines) do
            [
              'Failure/Error:',
              '  Resource::Project.fabricate_via_api! do |project|',
              '    project.visibility = :private',
              '  end'
            ]
          end

          it_behaves_like 'screenshot not included'
        end

        context 'when the report has an empty stacktrace' do
          let(:failure_lines) { [] }

          it_behaves_like 'issue created with failure class & message'
        end

        context 'when the issue title would be longer than the maximum allowed' do
          let(:test_file_partial) { 'api/3_create/gitaly/changing_repository_storage_spec.rb' }
          let(:test_name) do
            'Create Changing Gitaly repository storage when moving from Gitaly to Gitaly Cluster behaves like ' \
              'repository storage move confirms a `finished` status after moving project repository storage'
          end

          it 'creates the issue in the provided project and post the failed job url' do
            full_title = "Failure in #{test_file_partial} | #{test_name}"

            expect(subject.__send__(:gitlab)).to receive(:upload_file).and_return(file_upload)
            expect(subject.__send__(:gitlab)).to receive(:create_issue)
              .with(hash_including(
                title: "#{full_title[0...described_class::MAX_TITLE_LENGTH - 3]}...",
                issue_type: 'issue',
                labels: %w[QA Quality test failure::new priority::2 found:staging.gitlab.com]
              ))
              .and_return(new_issue)

            expect(subject).not_to receive(:update_reports)

            expect { subject.invoke! }.to output.to_stdout
          end
        end

        context 'when product group is specified' do
          let(:product_group_tag) { 'source_code' }
          let(:set_username) { 'jane_smith' }
          let(:user_id) { 23 }

          it 'assigns the issue to a SET and provides a due date' do
            expect(subject.__send__(:gitlab)).to receive(:upload_file).and_return(file_upload)
            expect(subject).to receive(:set_dri_via_group).and_return(set_username)
            expect_any_instance_of(GitlabQuality::TestTooling::GitlabIssueClient).to receive(:find_user_id).with(username: set_username).and_return(user_id)
            expect(subject.__send__(:gitlab)).to receive(:create_issue)
              .with(hash_including(
                title: "Failure in #{test_file_partial} | #{test_name}",
                issue_type: 'issue',
                labels: %w[QA Quality test failure::new priority::2 found:staging.gitlab.com],
                assignee_id: user_id,
                due_date: Date.today + 1.month
              ))
              .and_return(new_issue)
            expect(subject).not_to receive(:update_reports)

            expect { subject.invoke! }.to output.to_stdout
          end
        end
      end
    end

    context 'when one issue is found' do
      let(:issue) do
        issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', issue_title, issue_description)
      end

      let(:issues) { [issue] }

      before do
        allow(subject).to receive(:update_reports)
      end

      context 'when test name matches issue title' do
        let(:issue_title) { "Hello #{test_name} world!" }

        it 'finds the issue' do
          expect do
            subject.invoke!
          end.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
        end

        it 'adds the failed job url to the issue description' do
          expect(subject.__send__(:gitlab)).to receive(:edit_issue)
            .with(iid: anything, options: {
              labels: base_issue_labels + ['found:staging.gitlab.com'],
              description: match(ci_job_url)
            })
          expect(subject).to receive(:update_reports).and_call_original

          expect { subject.invoke! }.to output.to_stdout
        end
      end

      context 'when test path matches issue title' do
        let(:issue_title) { "Hello #{test_file_partial} world!" }

        it 'finds the issue' do
          expect do
            subject.invoke!
          end.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
        end
      end

      context 'when test error and stacktrace have unique identifiers' do
        let(:issue_title) { "Hello #{test_name} world!" }
        let(:failure_lines) { ['QA User 82f8df1886c8b619 -82f8df1886c8b619 qa-test-82f8df1886c8b619'] }
        let(:issues) do
          [issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name,
            issue_description.gsub(/```.+```/m,
              "```\nQA User 7f50df36c0708ec3 -7f50df36c0708ec3 qa-test-7f50df36c0708ec3\n```"))]
        end

        it 'finds the issue' do
          expect do
            subject.invoke!
          end.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
        end
      end

      context 'when test matches issue but stacktrace is too different' do
        let(:issue) do
          issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name,
            issue_description.gsub('click_submit', 'click_an_entirely_different_button_altogether'))
        end

        it 'creates the issue in the provided project' do
          expect(subject.__send__(:gitlab)).to receive(:upload_file).and_return(file_upload)
          expect(subject.__send__(:gitlab)).to receive(:create_issue)
            .with(
              title: "Failure in #{test_file_partial} | #{test_name}",
              description: expected_new_issue_description,
              labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'],
              issue_type: 'issue',
              confidential: false
            )
            .and_return(new_issue)

          expect { subject.invoke! }.to output.to_stdout
        end
      end
    end

    context 'when multiple issues are found' do
      # rubocop:disable RSpec/IndexedLet
      let(:issue1) do
        issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name, issue_description.gsub(/### Reports.*\z/m, ''))
      end

      let(:issue2) do
        issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description)
      end
      # rubocop:enable RSpec/IndexedLet

      let(:issues) { [issue1, issue2] }

      before do
        allow(subject).to receive(:update_reports)
      end

      it 'displays a warning when multiple issues are found and none is a better match than the other' do
        expect do
          subject.invoke!
        end.to output(/Too many issues found for test '#{test_name}' \(`#{test_file_full}`\)!/).to_stderr
        expect { subject.invoke! }.not_to output(/Found issue/).to_stdout
        expect { subject.invoke! }.not_to output(/Created new issue/).to_stdout
      end

      context 'when one issue is a better match than the other' do
        let(:issue2) do
          issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name,
            issue_description.gsub('click_submit', 'click_cancel'))
        end

        it 'returns the best matching issue' do
          expect do
            subject.invoke!
          end.to output(/Found issue #{issue1.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
        end

        it 'posts the failed job url' do
          expect(subject.__send__(:gitlab)).to receive(:edit_issue)
            .with(iid: anything, options: {
              labels: base_issue_labels + ['found:staging.gitlab.com'],
              description: match(ci_job_url)
            })
          allow(subject).to receive(:update_reports).and_call_original

          expect { subject.invoke! }.to output.to_stdout
        end
      end

      context 'when the report has an empty stacktrace' do
        let(:issue2) do
          issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name,
            issue_description.gsub(/```.+```/m, "```\nCapybara::ElementNotFound: #{failure_message}\n```"))
        end

        let(:test_data) do
          <<~JSON
            {
              "examples": [
                {
                  "full_description": "#{test_name}",
                  "file_path": "#{test_file_full}",
                  "exceptions":[
                    {
                      "class": "Capybara::ElementNotFound",
                      "message": "#{failure_message}",
                      "message_lines": [          ],
                      "backtrace": [          ]
                    }
                  ]
                }
              ]
            }
          JSON
        end

        it 'finds the issue' do
          expect do
            subject.invoke!
          end.to output(/Found issue #{issue2.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
        end
      end
    end

    context 'when issues with similar stack traces but different titles are found' do
      # rubocop:disable RSpec/IndexedLet
      let(:test_name1) { 'Govern Security Dashboard in a Project creates an issue from vulnerability details' }
      let(:test_name2) do
        'Govern Security Reports in a Merge Request Widget displays vulnerabilities in merge request widget'
      end

      let(:test_name3) do
        'Failure in browser_ui/8_monitor/alert_management/create_alert_using_authorization_key_spec.rb'
      end

      let(:failure_issue_labels) { %w[test failure::new QA Quality] }
      let(:issue1) do
        issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name1, "#{issue_description}\n#{reports_section}",
          failure_issue_labels)
      end

      let(:issue2) do
        issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name2, "#{issue_description}\n#{reports_section}",
          failure_issue_labels)
      end

      let(:issue3) do
        issue_class.new(3, 'http://existing-issue.url/issue3', 'opened', test_name3, "#{issue_description}\n#{reports_section}",
          failure_issue_labels)
      end
      # rubocop:enable RSpec/IndexedLet

      before do
        allow(subject).to receive(:pipeline).and_return("main")
      end

      context 'with number of similar issues exceeding spam threshold count of 3' do
        let(:issues) { [issue1, issue2, issue3] }

        it 'does NOT create a new issue for test failure' do
          expect(subject.__send__(:gitlab)).not_to receive(:create_issue)
          issues.each do |issue|
            expect(subject).to receive(:update_reports).with(issue, anything)
          end

          expect { subject.invoke! }
            .to output(/=> Similar failure issues have already been opened for the same pipeline environment/).to_stdout
        end
      end

      context 'with number of similar issues below the spam threshold count of 3' do
        let(:issues) { [issue1, issue2] }

        it_behaves_like 'new issue created'
      end

      context 'with similar issues but one of them without job url in the description' do
        let(:description_without_job_url) { issue_description.gsub(/### Reports.*\z/m, "") }
        let(:issue_without_job_url) do
          issue_class.new(3, 'http://existing-issue.url/issue3', 'opened', test_name3,
            description_without_job_url, failure_issue_labels)
        end

        let(:issues) { [issue1, issue2, issue_without_job_url] }

        before do
          [issue1, issue2].each do |issue|
            allow(subject).to receive(:update_reports).with(issue, anything)
          end
        end

        it_behaves_like 'new issue created'
      end

      context 'with test pipeline environment different to failure issues pipeline environment' do
        before do
          allow(subject).to receive(:failed_issue_job_url).and_return("https://ops.gitlab.net/gitlab-org/quality/production/-/jobs/9392299")
        end

        let(:issues) { [issue1, issue2, issue3] }

        it_behaves_like 'new issue created'
      end
    end

    context 'when the test is quarantined' do
      let(:test_data) do
        <<~JSON
          {
            "examples": [
              {
                "id":"#{test_file_full}[1:1:1]",
                "description":"GET /users",
                "full_description": "#{test_name}",
                "status":"failed",
                "file_path":"#{test_file_full}",
                "pending_message":null,
                "testcase":"#{testcase_url}",
                "quarantine":{
                  "type": "bug",
                  "issue": "https://gitlab.com/quarantine-issue/123"
                },
                "screenshot":{},
                "product_group":null,
                "ci_job_url":"#{ci_job_url}",
                "exceptions":[
                  {
                    "class":"RSpec::Expectations::ExpectationNotMetError",
                    "message":"\\nexpected: 404\\n     got: 200\\n\\n(compared using ==)\\n",
                    "message_lines":#{failure_lines.to_json},
                    "backtrace":[
                      "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                      "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                    ]
                  }
                ]
              }
            ]
          }
        JSON
      end

      it 'does not create a new issue' do
        expect(subject.__send__(:gitlab)).not_to receive(:create_issue)
        expect(subject).not_to receive(:update_reports)

        expect { subject.invoke! }.to output.to_stdout
      end

      context 'when there is an existing failure comment' do
        let(:issue_title) { "Hello #{test_name} world!" }
        let(:note_class) { Struct.new(:id, :body) }

        let(:issue) do
          issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', issue_title, issue_description)
        end

        let(:issues) { [issue] }

        it 'updates the existing issue description with the failed job pipeline and url' do
          expect(subject.__send__(:gitlab)).to receive(:edit_issue)
            .with(iid: anything, options: {
              labels: base_issue_labels + ["quarantine", "quarantine::bug", 'found:staging.gitlab.com'],
              description: match(ci_job_url)
            })
          expect(subject).to receive(:update_reports).and_call_original

          expect { subject.invoke! }.to output.to_stdout
        end
      end
    end

    context 'when systemic exception is detected' do
      let(:failure_message) { "PG::ConnectionBad: PQsocket() can't get socket descriptor\\nfoo" }
      let(:failure_reason) { "`Support::Repeater::WaitExceededError: PG::ConnectionBad: PQsocket() can't get socket descriptor`" }

      before do
        stub_const("#{described_class}::SYSTEMIC_EXCEPTIONS_THRESHOLD", 1)
      end

      it_behaves_like 'error(s) can be ignored'
    end

    context 'when the error can be ignored' do
      let(:failure_message) { 'Net::ReadTimeout' }
      let(:failure_reason) { "`#{failure_message}`" }

      it_behaves_like 'error(s) can be ignored'
    end

    context 'with multiple errors' do
      let(:failure_message) { 'Error' }
      let(:failure_message2) { 'Error' }
      let(:test_data) do
        <<~JSON
          {
            "examples": [
              {
                "full_description": "#{test_name}",
                "file_path": "#{test_file_full}",
                "screenshot": { "image": "failure_screenshot.png"},
                "exceptions": [
                  {
                    "class": "Failure Class",
                    "message": "An Error Here #{failure_message}",
                    "message_lines": #{failure_lines.to_json},
                    "backtrace":[
                      "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                      "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                    ]
                  },
                  {
                    "class": "Failure Class",
                    "message": "An Error Here #{failure_message2}",
                    "message_lines": #{failure_lines.to_json},
                    "backtrace":[
                      "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                      "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                    ]
                  }
                ]
              }
            ]
          }
        JSON
      end

      context 'when all errors can be ignored' do
        let(:failure_message) { 'Net::ReadTimeout' }
        let(:failure_message2) { '403 Forbidden - Your account has been blocked' }
        let(:failure_reason) { "`#{failure_message}`, `#{failure_message2}`" }

        it_behaves_like 'error(s) can be ignored'
      end

      context 'when only one error can be ignored' do
        let(:failure_message) { 'Net::ReadTimeout' }

        it_behaves_like 'new issue created'
      end

      context 'when no error can be ignored' do
        it_behaves_like 'new issue created'
      end
    end

    describe 'computing stacktrace diffs' do
      it 'computes a diff ratio for a string' do
        ratio = subject.send(:calculate_diff_ratio, 'pattern', 'pattren')
        expect(ratio).to eq(0.286)
      end
    end
  end
end
