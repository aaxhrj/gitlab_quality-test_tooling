# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    module GitlabClient
      class MergeRequestDryClient < MergeRequest
        def find_merge_request
          puts "Finding merge_request_id #{merge_request_iid}"
          puts "project: #{project}"
        end

        def merge_request_changed_files
          puts "Changed files for #{merge_request_iid}"
          []
        end

        def find_note(body:)
          puts "Find note for #{merge_request_iid} with body: #{body}"
        end

        def create_note(note:)
          puts "The following note would have been created with body: #{note}"
        end

        def update_note(id:, note:)
          puts "The following note would have been update id: #{id} with body: #{note}"
        end
      end
    end
  end
end
