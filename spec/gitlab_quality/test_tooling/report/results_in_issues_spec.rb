# frozen_string_literal: true

# rubocop:disable RSpec/VerifiedDoubles
RSpec.describe GitlabQuality::TestTooling::Report::ResultsInIssues do
  let(:results_issue_project) { 'valid-results-issue-project' }
  let(:test_file_partial) { 'browser_ui/stage/test_spec.rb' }
  let(:test_file_path) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
  let(:test_description) { 'test-name' }
  let(:test_status) { 'failed' }
  let(:test) do
    GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
      'file_path' => test_file_path,
      'full_description' => test_description,
      'status' => test_status
    )
  end

  let(:test_failures) do
    [
      {
        'message' => "Exception: error message",
        'message_lines' => 'message lines',
        'stacktrace' => "message lines\nbacktrace1\nbacktrace2"
      }
    ]
  end

  let(:testcase_description) { "#{subject.__send__(:new_issue_description, test)}\n\n" }

  let(:testcase) do
    double('Test case', description: testcase_description)
  end

  let(:result_issue_iid) { 99 }
  let(:result_issue_url) { "http://result-issue.url/-/issues/#{result_issue_iid}" }
  let(:result_issue_title) { subject.__send__(:title_from_test, test) }
  let(:result_issue_labels) { %w[Quality status::automated staging::passed] }
  let(:result_issue) do
    double('Result issue',
      iid: result_issue_iid,
      title: result_issue_title,
      description: subject.__send__(:new_issue_description, test).to_s,
      labels: result_issue_labels,
      web_url: result_issue_url)
  end

  before do
    allow(test).to receive(:failures).and_return(test_failures)
  end

  subject do
    described_class.new(token: 'token', input_files: 'files', project: results_issue_project)
  end

  describe '#get_related_issue' do
    context 'when a result issue is linked in the given test case' do
      let(:testcase_description) { "#{described_class::TEST_CASE_RESULTS_SECTION_TEMPLATE}: #{result_issue_url}" }

      it 'fetches test case from the API' do
        expect(subject.__send__(:gitlab))
          .to receive(:find_issues).with(iid: result_issue_iid)
          .and_return([result_issue])

        subject.get_related_issue(testcase, test)
      end

      context 'when test case title needs update' do
        let(:result_issue_title) { 'old title' }

        it 'fetches test case from the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues).with(iid: result_issue_iid)
            .and_return([result_issue])
          expect(subject.__send__(:gitlab))
            .to receive(:edit_issue).with(
              iid: result_issue_iid,
              options: {
                title: subject.__send__(:title_from_test, test)
              }
            )
            .and_return([result_issue])

          expect { subject.get_related_issue(testcase, test) }
            .to output(/issue title needs to be updated from 'old title' to '.+'/).to_stderr
        end
      end
    end

    context 'when no result issue is linked in the given test case' do
      context 'when an issue is found' do
        it 'fetches test case issue from the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues).with(options: { search: %("#{test_file_partial}" "#{test_description}") })
            .and_return([result_issue])

          expect { subject.get_related_issue(testcase, test) }
            .to output("No valid issue link found.\nFound existing issue: #{result_issue_url}\n").to_stdout
        end
      end

      context 'when no issue is found' do
        let(:new_issue) { double('Issue', web_url: 'path/to/issues/123') }

        it 'creates a new test case via the API' do
          expect(subject.__send__(:gitlab))
            .to receive(:find_issues)
            .and_return([])
          expect(subject.__send__(:gitlab))
            .to receive(:create_issue).with(
              title: result_issue_title,
              description: subject.__send__(:new_issue_description, test),
              labels: %w[Quality status::automated],
              issue_type: 'issue',
              confidential: false
            )
            .and_return(new_issue)

          expect { subject.get_related_issue(testcase, test) }
            .to output("No valid issue link found.\nCreated new issue: #{new_issue.web_url}\n").to_stdout
        end
      end
    end
  end

  describe '#update_issue_labels' do
    around do |example|
      ClimateControl.modify(CI_PROJECT_NAME: 'staging') { example.run }
    end

    it 'updates the result issue labels' do
      expect(subject.__send__(:gitlab))
        .to receive(:edit_issue).with(
          iid: result_issue_iid,
          options: {
            labels: ['Quality', 'status::automated', 'Testcase Linked', 'staging::failed']
          }
        )
        .and_return([result_issue])

      expect(subject.update_issue_labels(result_issue, test)).to be_truthy
    end
  end

  describe '#post_note' do
    let(:note_content) { "Error:\n```\nException: error message\n```\n\nStacktrace:\n```\nmessage lines\nbacktrace1\nbacktrace2\n```" }

    around do |example|
      ClimateControl.modify(
        CI_PROJECT_NAME: 'staging',
        CI_JOB_NAME: 'job_name',
        CI_JOB_URL: 'job_url'
      ) { example.run }
    end

    shared_examples 'no discussion created' do
      it 'does not create a discussion' do
        expect(subject.__send__(:gitlab))
        .not_to receive(:find_issue_discussions)

        expect(subject.post_note(result_issue, test)).to be_falsy
      end
    end

    context 'when test was skipped' do
      let(:test_status) { 'pending' }

      it_behaves_like 'no discussion created'
    end

    context 'when test has no failures' do
      before do
        allow(test).to receive(:failures).and_return([])
      end

      it_behaves_like 'no discussion created'
    end

    context 'when no matching discussion is found' do
      let(:note) { %(:x: ~"staging::failed" in job `job_name` in job_url\n\n#{note_content}\n) }

      it 'creates a new discussion' do
        expect(subject.__send__(:gitlab))
          .to receive(:find_issue_discussions).with(iid: result_issue_iid)
          .and_return([])
        expect(subject.__send__(:gitlab))
          .to receive(:create_issue_note).with(iid: result_issue_iid, note: note)
          .and_return(double('Note'))

        expect(subject.post_note(result_issue, test)).to be_truthy
      end
    end

    context 'when a matching discussion is found' do
      let(:discussion_notes) { [{ 'body' => note_content }] }
      let(:discussion) { double('Discussion', id: 111, notes: discussion_notes) }
      let(:note) { %(:x: ~"staging::failed" in job `job_name` in job_url) }

      it 'adds a note to the existing discussion' do
        expect(subject.__send__(:gitlab))
          .to receive(:find_issue_discussions).with(iid: result_issue_iid)
          .and_return([discussion])
        expect(subject.__send__(:gitlab))
          .to receive(:add_note_to_issue_discussion_as_thread).with(
            iid: result_issue_iid,
            discussion_id: discussion.id,
            body: note)
          .and_return(double('Note'))

        expect(subject.post_note(result_issue, test)).to be_truthy
      end
    end
  end
end
# rubocop:enable RSpec/VerifiedDoubles
