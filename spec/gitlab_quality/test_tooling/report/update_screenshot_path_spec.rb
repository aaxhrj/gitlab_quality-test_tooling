# frozen_string_literal: true

require 'json'

RSpec.describe GitlabQuality::TestTooling::Report::UpdateScreenshotPath do
  describe '#invoke!' do
    it 'requires input input_files' do
      expect { subject.invoke! }.to raise_error(ArgumentError, "missing keyword: :input_files")
    end

    it 'accepts input input_files' do
      input_files = 'input_files'
      subject = described_class.new(input_files: input_files)

      allow(Dir).to receive(:glob).and_return([])

      expect { subject.invoke! }.not_to raise_error
    end

    describe 'with input input_files' do
      subject { described_class.new(input_files: 'input_files') }

      it 'replaces screenshot path in both xml and json reports' do
        file_path = '/some_path/gitlab-qa-run-2020-04-09-22-54-33-69582143/gitlab-ee-qa-f4399756/rspec-505959567.xml'
        json_file_path = '/some_path/gitlab-qa-run-2020-04-09-22-54-33-69582143/gitlab-ee-qa-f4399756/rspec-505959567.json'

        input_system_out = <<~REPORT
          <?xml version="1.0"?>
          <system-out>[[ATTACHMENT|/home/gitlab/qa/tmp/qa-test-2020-04-09-23-02-42-0d1f390c65be6dd7/manage_basic user login.png]]</system-out>
        REPORT

        expected_system_out = <<~REPORT
          <?xml version="1.0"?>
          <system-out>[[ATTACHMENT|/some_path/gitlab-qa-run-2020-04-09-22-54-33-69582143/gitlab-ee-qa-f4399756/qa-test-2020-04-09-23-02-42-0d1f390c65be6dd7/manage_basic user login.png]]</system-out>
        REPORT

        input_json_path = <<~JSON
          {
            "examples": [
              {
                "screenshot": {
                  "image": "/home/gitlab/qa/tmp/qa-test-2020-04-09-23-02-42-0d1f390c65be6dd7/manage_basic_user_login.png"
                }
              }
            ]
          }
        JSON

        expected_json_path = <<~JSON
          {
            "examples": [
              {
                "screenshot": {
                  "image": "/some_path/gitlab-qa-run-2020-04-09-22-54-33-69582143/gitlab-ee-qa-f4399756/qa-test-2020-04-09-23-02-42-0d1f390c65be6dd7/manage_basic_user_login.png"
                }
              }
            ]
          }
        JSON

        expected_json_path = JSON.parse(expected_json_path)

        allow(Dir).to receive(:glob).and_return([file_path])

        expect(File).to receive(:open).with(file_path).and_return(input_system_out)
        expect(File).to receive(:write).with(file_path, expected_system_out)
        expect(File).to receive(:read).with(json_file_path).and_return(input_json_path)
        expect(File).to receive(:write).with(json_file_path, JSON.pretty_generate(expected_json_path))

        expect { subject.invoke! }.to output.to_stdout
      end
    end
  end
end
