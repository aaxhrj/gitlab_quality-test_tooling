# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling do
  it "has a version number" do
    expect(GitlabQuality::TestTooling::VERSION).not_to be_nil
  end
end
