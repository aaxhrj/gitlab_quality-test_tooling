# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::GenerateTestSession do
  let(:test_data_path) { 'test_data_path' }
  let(:test_data) do
    <<~JSON
      {
        "examples": [
          {
            "full_description": "test-name-01",
            "file_path": "#{file_path_base}/test_01_spec.rb",
            "testcase": "#{testcase}/1",
            "ci_job_url": "/10",
            "status": "passed",
            "quarantine": null
          },
          {
            "full_description": "test-name-02",
            "file_path": "#{file_path_base}/test_02_spec.rb",
            "testcase": "#{testcase}/2",
            "ci_job_url": "/20",
            "status": "pending"
          },
          {
            "full_description": "test-name-03",
            "file_path": "#{file_path_base}/test_03_spec.rb",
            "testcase": "#{testcase}/3",
            "ci_job_url": "/30",
            "status": "failed"
          },
          {
            "full_description": "test-name-04",
            "file_path": "#{file_path_base}/test_04_spec.rb",
            "testcase": "#{testcase}/4",
            "ci_job_url": "/40",
            "status": "unknown"
          },
          {
            "full_description": "test-name-05",
            "file_path": "qa/specs/features/browser_ui/plan/test_05_spec.rb",
            "testcase": "#{testcase}/5",
            "ci_job_url": "/50",
            "status": "passed",
            "quarantine": "https://gitlab.com/gitlab-org/gitlab/-/issues/12345"
          },
          {
            "full_description": "test-name-06",
            "file_path": "#{file_path_base}/test_01_spec.rb",
            "testcase": "#{testcase}/1",
            "ci_job_url": "/60",
            "status": "passed"
          },
          {
            "full_description": "test-name-03",
            "file_path": "#{file_path_base}/test_03_spec.rb",
            "testcase": "#{testcase}/3",
            "ci_job_url": "/70",
            "status": "failed",
            "quarantine": { "issue": "https://gitlab.com/gitlab-org/gitlab/-/issues/12345" }
          },
          {
            "full_description": "test-name-02",
            "file_path": "#{file_path_base}/test_02_spec.rb",
            "testcase": "#{testcase}/2",
            "ci_job_url": "/80",
            "status": "crashed"
          },
          {
            "full_description": "test-name-07",
            "file_path": "#{file_path_base}/test_07_spec.rb",
            "testcase": "#{testcase}/7",
            "ci_job_url": "/90",
            "status": "failed",
            "failure_issue": "/failures/90"
          },
          {
            "full_description": "test-name-07",
            "file_path": "#{file_path_base}/test_07_spec.rb",
            "testcase": "#{testcase}/7",
            "ci_job_url": "/90",
            "status": "failed",
            "failure_issue": "/failures/90"
          },
          {
            "full_description": "test name 08 with spaces",
            "file_path": "#{file_path_base}/test_08_spec.rb",
            "testcase": "#{testcase}/8",
            "ci_job_url": "/81",
            "status": "failed",
            "failure_issue": "/failures/81"
          }
        ]
      }
    JSON
  end

  let(:file_path_base) { 'qa/specs/features/browser_ui/stage' }
  let(:testcase) { 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases' }

  let(:env) do
    {
      'CI_PROJECT_NAME' => 'staging',
      'CI_PIPELINE_URL' => 'ci_pipeline_url',
      'CI_PIPELINE_ID' => 'ci_pipeline_id',
      'CI_PROJECT_ID' => 'ci_project_id',
      'DEPLOY_VERSION' => 'deadbeef',
      'DEPLOY_ENVIRONMENT' => 'gstg',
      'GITLAB_QA_ISSUE_URL' => 'https://example.com/qa/issue',
      'QA_RUN_TYPE' => 'e2e-package-and-test'
    }
  end

  subject do
    described_class.new(
      token: 'token',
      ci_project_token: 'ci_project_token',
      project: 'project',
      input_files: [double],
      confidential: confidential
    )
  end

  shared_examples 'a new test session issue' do
    it 'creates an issue as test session report' do
      description = <<~MARKDOWN.chomp
      ## Session summary

      * Deploy version: deadbeef
      * Deploy environment: gstg
      * Pipeline: staging [ci_pipeline_id](ci_pipeline_url)
      * Total 11 tests
      * Passed 3 tests
      * Failed 5 tests
      * 3 other tests (usually skipped)

      ## Failed jobs

      * [A](AURL) (allowed to fail)
      * [B](BURL)

      ### Plan

      * Total 1 tests
      * Passed 1 tests
      * Failed 0 tests
      * 0 other tests (usually skipped)

      <details><summary>Passed tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      test-name-05 | [50](/50) ~"quarantine" | ~"passed" | -

      </details>

      ### Stage

      * Total 10 tests
      * Passed 2 tests
      * Failed 5 tests
      * 3 other tests (usually skipped)

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-03](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases?state=opened&search=test-name-03) | [30](/30), [70](/70) ~"quarantine" | ~"failed" | <ul><li>[ ] failure issue exists or was created</li></ul>
      [test-name-07](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases?state=opened&search=test-name-07) | [90](/90) | ~"failed" | <ul><li>[ ] [failure issue](/failures/90)</li></ul>
      [test name 08 with spaces](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases?state=opened&search=test%20name%2008%20with%20spaces) | [81](/81) | ~"failed" | <ul><li>[ ] [failure issue](/failures/81)</li></ul>

      <details><summary>Passed tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      test-name-01, test-name-06 | [10](/10), [60](/60) | ~"passed" | -

      </details>

      <details><summary>Other tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-02](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases?state=opened&search=test-name-02) | [20](/20), [80](/80) | ~"pending", ~"crashed" | -
      [test-name-04](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases?state=opened&search=test-name-04) | [40](/40) | ~"unknown" | -

      </details>

      ## Release QA issue

      * https://example.com/qa/issue

      /relate https://example.com/qa/issue

      ## Link to Grafana dashboard for run-type of e2e-package-and-test

      * https://dashboards.quality.gitlab.net/d/tR_SmBDVk/main-runs?orgId=1&refresh=1m&var-run_type=e2e-package-and-test
      MARKDOWN

      stub_request(:post, 'https://gitlab.com/api/v4/projects/project/issues')
        .with(headers:
        {
          'Private-Token' => 'token'
        })
        .with(body:
        {
          'title' => "#{Time.now.strftime('%Y-%m-%d')} Test session report | #{env['QA_RUN_TYPE']}",
          'description' => description,
          'issue_type' => 'issue',
          'labels' => ['Quality', 'QA', 'triage report', 'found:staging.gitlab.com'],
          'confidential' => confidential
        })
        .to_return(body: { 'web_url' => 'issue_url', 'iid' => 4 }.to_json)

      stub_request(:post, 'https://gitlab.com/api/v4/projects/project/issues/4/notes')
        .with(headers:
        {
          'Private-Token' => 'token'
        })
        .with(body:
        {
          'body' => '/relate https://example.com/qa/issue'
        })

      expect { subject.invoke! }.to output.to_stdout
      expect(subject.invoke!).to eq('issue_url')
    end
  end

  before do
    allow(subject).to receive(:validate_input!)
    allow(Dir).to receive(:glob).and_return([test_data_path])
    allow(File).to receive(:read).and_return(test_data)

    stub_request(:get, 'https://gitlab.com/api/v4/projects/ci_project_id/pipelines/ci_pipeline_id/jobs?scope=failed')
      .with(headers:
      {
        'Private-Token' => 'ci_project_token'
      })
      .to_return(body:
      [
        { allow_failure: true, name: 'A', web_url: 'AURL' },
        { allow_failure: false, name: 'B', web_url: 'BURL' }
      ].to_json)
  end

  around do |example|
    ClimateControl.modify(env) { example.run }
  end

  describe '#invoke!' do
    context 'when confidential is true' do
      it_behaves_like 'a new test session issue' do
        let(:confidential) { true }
      end
    end

    context 'when confidential is false' do
      it_behaves_like 'a new test session issue' do
        let(:confidential) { false }
      end
    end
  end
end
